# Okta Blog Tutorial

## Description
This is a re-expression of the Okta lesson by Randall Degges completed in 2018 but for use in July 2020. That article is courtesy of the [Okta blog](https://developer.okta.com/blog/2018/06/28/tutorial-build-a-basic-crud-app-with-node "link to Express CRUD app").

### Processing and/or completion date(s)
June 30, 2020 to July 05, 2020

## &quot;What is different?&quot; you ask&hellip;
Hmmm&hellip; Not much, really! But it works really good (if I can say so myself). So&hellip; HEH!!!

The difference(s) is/are in the versions utilized for this exercise. I posited early on that in order to keep things consistent it&rsquo;d be best to use **sequelize@latest** and **sequelize-cli@latest** whenever possible (the latter does not apply in this case), then, expand from there. But in terms of proprietary software such as those that belong to and are formulated by Okta, then, I felt it would be imperative to stick with the self&#45;same version that is utilized in said lesson. In their words, **breaking changes** can come about if you venture off of the path. That&rsquo;s just reality sometimes. :)

There are five (5) **other** things of which you should be aware:
  - First, I applied a name attribute to the express-session object and I pull the value from the dot env file. I drew from a previous semi-related lesson for reasons which will become clear. This was necessary so that a reference could be made to the **/users/logout** route to that cookie so that it would be properly destroyed not only when session is closed. More importantly the cookie should be destroyed upon using the logout. See the below illustration:

```sh
router.get('/logout', (req, res, next)  => {
  
  # Lookie here!!!
  req.session.destroy( err => {
    if( err ) return res.redirect('/dashboard');
    req.logout();
    # Lookie here, too!!!
    res.clearCookie( SESS_NAME );
    res.redirect("/");
  } );

});
```

  - Second, I prefer to keep Bootstrap, JQuery and @popperjs on hand rather than make external calls to other third-party assets. Each **public/third-party/** and **views/layout.pug** are illustrative of this.
  - Next, I use object deconstruction whereever possible. This is used when importing a) the helper functions; b) the Post model; and, most important of all when assigning the Sequelize package variable to a **symbol** and not to a **string**. If you review each **./helpers.js** and **./models.js** you will see what I am talking about, too.
  - Last, it became clear that **session configuration object attribute key and value** had to be changed to **resave: false**.

## Handling Sign-in Derrpage

This is important, too! It may have been the case that "my wires were crossed" while configuring the Okta application but in case you see derrpage [like the one mentioned at the Okta support page ](https://support.okta.com/help/s/question/0D50Z00008G7VdzSAF/the-redirecturi-parameter-must-be-an-absolute-uri-that-is-whitelisted-in-the-client-app-settings "link to support page"), just know that **Mircea Baciu's response is the correct answer.**

I could neither upvote nor leave a comment, thus, I will elaborate here&hellip;

Chrome works because it is a very permissive browser. Higher and stricter than normal security settings within the context of a browser, indeed, will affect the assignment of third party cookies. 

In contrast, Firefox has the option to enable strict Browser Privacy. For example, if you navigate to "Edit" >>> "Preferences" >>> "Privacy & Security" (or "about:preferences#privacy" for short) you should be presented with your browser privacy options. 

In theory, since I still have selected "Strict" as opposed to "Standard" selected I should be precluded from gaining access to my login screen courtesy of Okta (unless I am logged in to the administrator's dashboard).

If I decide, however, to accommodate the Okta sign-in api, then, I am 99.99999999999999999999999% that things will work out as expected (because as we used to say in Falls Church there is always that "gnat's ass hair"-- we used that phrase to refer to a very, very small quantity --of a possibility that things will not work out according to plan").

**Thank you for your help, Mircea.** It was golden!


## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the ApolloGraphQL fullstack tutorial (or, **AFT** for short) on two separate occasions. The authors **purported** that one does not need to know the other minor themes in order to complete the lesson. That would be nice if it were true, however, it was not meant to be so.

When I am fortunate enough to undertake tutorials and lessons by third&#45;parties I usually prefer to hammer them out with the keyboard and go through the motions as opposed to merely blindly copying and pasting code (in addition to consulting API references and guides).

In terms of the **AFT**, it became obvious that some of the technologies were archaic, possibly deprecated &amp; likely containing security vulnerabilities. 

Therefore, in order to follow and ultimately complete the **AFT** to fruition, I decided to embark on learning and/or shoring-up some of the minor themes involved such as Sqlite3 and Sequelize (which I had been meaning to do for quite some time). 

Personally, I think my plan was a success. The problem was never with learning GraphQL and/or ApolloGraphQL. The bugs I was sure at the time were limited to (again) the minor themes. I was familiar with WebSQL for browsers but Sqlite3 would prove to be different. And, I had seen Sequelize from afar when researching other subjects such as MERN and MEAN stacks. I could not procrastinate with learning Sequelize any more. I am thrilled to have finally started practicing with Sequelize. **Yes!!!**


## God bless!
Please help me if you are able. Thank you for your visit. 