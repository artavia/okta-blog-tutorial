const { Sequelize } = require("sequelize");

const configObj = {
  dialect: "sqlite"
  , storage: "./database.sqlite3"
};

const db = new Sequelize( configObj );

// Only let the user access the route if they are authenticated.
const ensureAuthenticated = (req, res, next) => {
  if(!req.user){
    return res.status(401).render("unauthenticated");
  }
  next();
};

// Return a DB instance
const getDB = () => {
  return db;
};

module.exports = { ensureAuthenticated, getDB };