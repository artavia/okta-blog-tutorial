// =============================================
// ADDITIONAL BOILERPLATE 
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
} 


const { 
  
  NODE_ENV
  , OKTA_ORG_URL
  , API_TOKEN_VALUE

} = process.env;

const okta = require("@okta/okta-sdk-nodejs");

const client = new okta.Client( {
  orgUrl: OKTA_ORG_URL
  , token: API_TOKEN_VALUE
} );

module.exports = { client };