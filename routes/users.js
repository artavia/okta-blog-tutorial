// =============================================
// ADDITIONAL BOILERPLATE 
// =============================================
if( process.env.NODE_ENV !== "production" ){ 
  const dotenv = require("dotenv"); 
  dotenv.config();
} 

const { 
  SESS_NAME
} = process.env;


const express = require("express");
const router = express.Router();


router.get("/logout", (req, res, next)  => {
  
  req.session.destroy( err => { 
    
    if( err ) return res.redirect("/dashboard");

    req.logout(); 
    res.clearCookie( SESS_NAME ); // my addition
    res.redirect("/"); 

  } ); // my addition, too

} );

module.exports = router;
